'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
        'Answers',
        [
            {
                userId: 2,
                questionId: 1,
                text: "Answer first question....",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                userId: 1,
                questionId: 2,
                text: "Answer second question...",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ],
        {},
    ),

    down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Answers', null, {}),
};
