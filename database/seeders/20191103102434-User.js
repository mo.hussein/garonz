'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
        'Users',
        [
            {
                name: 'Mohamed Hussein',
                email: 'M.Hussein@garonz.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                name: 'Amr Negm',
                email: 'A.Negm@garonz.com',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ],
        {},
    ),

    down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Users', null, {}),
};
