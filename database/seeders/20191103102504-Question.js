'use strict';

module.exports = {
   up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
        'Questions',
        [
            {
                userId: 1,
                text: "First Question dummy?",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                userId: 2,
                text: "Second Question dummy?",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ],
        {},
    ),

    down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Questions', null, {}),
};
