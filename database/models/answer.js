'use strict';
module.exports = (sequelize, DataTypes) => {
  const Answer = sequelize.define('Answer', {
    text: DataTypes.TEXT,
    userId: DataTypes.INTEGER,
    questionId: DataTypes.INTEGER
  }, {});
  Answer.associate = function(models) {
    // associations can be defined here
      Answer.belongsTo(models.Question, {
          foreignKey: 'questionId',
          as: 'question'
      });

    Answer.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user'
    })
  };
  return Answer;
};