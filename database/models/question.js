'use strict';
module.exports = (sequelize, DataTypes) => {
  const Question = sequelize.define('Question', {
    text: DataTypes.TEXT,
    userId: DataTypes.INTEGER
  }, {});
  Question.associate = function(models) {
    // associations can be defined here
    Question.hasMany(models.Answer, {
        foreignKey: 'questionId',
        as: 'answers',
        onDelete: 'CASCADE',
    });

    Question.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user',
        onDelete: 'CASCADE',
    })
  };
  return Question;
};