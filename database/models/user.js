'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
      User.hasMany(models.Question, {
          foreignKey: 'userId',
          as: 'questions',
          onDelete: 'CASCADE',
      });

      User.hasMany(models.Answer, {
          foreignKey: 'userId',
          as: 'answers',
          onDelete: 'CASCADE',
      });
  };
  return User;
};