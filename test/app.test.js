var expect  = require('chai').expect;
var request = require('request');

it('Main page content', function(done) {
    request('http://localhost:3000/api' , function(error, response, body) {
        expect(body).to.equal('Welcome');
        done();
    });
});

it('Get list of users', function(done) {
    request('http://localhost:3000/api/users' , function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    });
});