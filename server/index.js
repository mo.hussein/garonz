const express = require('express');
const routes = require('../routes');

const server = express();
server.use(express.json());

server.use('/api', routes);

//Swagger for api docs
var swaggerOptions = require('../config/swagger.js');
const expressSwagger = require('express-swagger-generator')(server);
expressSwagger(swaggerOptions);


//Start Error Handling from all requests (middleware)
server.use((req, res, next)=>{
    const error = new  Error('Not Found');
    error.status = 404;
    next(error);
});

server.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
            message: error.message
        }
    });
});
//End of Error Handling

module.exports = server;
