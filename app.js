async function getUsers(req, res) {
  let {id, sort_field: sortField, page, limit} = req.params;
  if (id === undefined) {
    const user = await User.findOne({
      where: {
        id
      },
      attributes: ['name','email','id',
        [
          // todo: Use a stored field that is being updated with every new answer
          sequelize.literal('(SELECT COUNT(DISTINCT(Answer.questionId)) FROM Answer WHERE Answer.userId = User.id)'),
          'noOfQuestions',
        ],
      ]
    });
    res.status(200).send({users: [user]});
  } else {
    User.findAll({
  order: [sortField],
  attributes: ['name','email','id',
    [sequelize.literal('(SELECT COUNT(DISTINCT(Answer.questionId)) FROM Answer WHERE Answer.userId = User.id)'),'noOfQuestions',],
  ]
    }).then(users => {
      return res.status(200).send({users})
    });
  }
}

const answerQuestion = async (req, res) => {
const {userId, questionId, text} = req.body;
const answer = await Answer.create(userId, questionId, text)
res.status(200).send({answerId: answer.id})
};

const sendConfirmationEmail = async function(req, res) {
  const {userId} = req.params
  const user = await User.findOne({
    attributes: ['email'],
  });
const key = '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';
  sendGrid.setApiKey(key);
sendGrid.send({
  to: user.email,  from: 'storyfile@storyfile.com',
  subject: 'Answer Confirmation',  text: 'A new answer has been successfully submitted!'
});
  return res.send({message: 'message sent'})
}