const models = require('../database/models');
const sendGrid = require('@sendgrid/mail');
const getUserById = async (req, res) => {
    let {sort_field: sortField, page, limit} = req.query;
    let id = req.params.id;
    try {
        const user = await models.User.findOne({
            where: { id: id },
            attributes: [
                'name','email','id',
            ],
            include: [
                {
                    model: models.Answer,
                    as: 'answers',
                    distinct: true,
                    col: 'questionId',
                    attributes:[
                        'text',
                        [models.Answer.sequelize.fn('COUNT', models.Answer.sequelize.col('questionId')), 'noOfQuestions']
                    ]
                }
            ],
            group: ['User.id', 'answers.id']
        });
        if (user) {
            return res.status(200).json({ user });
        }
        return res.status(404).send('User with the specified ID does not exists');
    } catch (error) {
        return res.status(500).send(error.message);
    }
}

const getUsers = async (req, res) => {
    let {sort_field: sortField, page, limit} = req.query;
    if(sortField === undefined) sortField='id'
        try {
            const users = await models.User.findAll({
                attributes: [
                    'name','email','id',
                ],
                order: [sortField],
                include: [
                    {
                        model: models.Answer,
                        as: 'answers',
                        distinct: true,
                        col: 'questionId',
                        attributes:[
                            'text',
                            [models.Answer.sequelize.fn('COUNT', models.Answer.sequelize.col('questionId')), 'noOfQuestions']
                        ]
                    }
                ],
                group: ['User.id', 'answers.id']
            });
            if (users) {
                return res.status(200).json({users, message:"Get Users"});
            }
            return res.status(404).send('Error');
        } catch (error) {
            return res.status(500).send(error.message);
        }

}

const sendConfirmationEmail = async function(req, res) {
    const {userId} = req.params
    const user = await User.findOne({
        where: { id: userId },
        attributes: ['email'],
    });
    if(user){
        try {
            const key = process.env.SENDGRID_API_KEY;
            sendGrid.setApiKey(key);
            const msg = {
                to: user.email,
                from: 'Answer Confirmation',
                subject: 'Sending with Twilio SendGrid is Fun',
                text: 'A new answer has been successfully submitted!'
            };
            sendGrid.send(msg);
            return res.send({message: 'message sent'})
        }catch (error) {
            return res.status(500).send(error.message);
        }
    }else{
        return res.status(404).send('User with the specified ID does not exists');
    }

} //Send Email using sendGrid

module.exports = {
    getUserById,
    getUsers,
    sendConfirmationEmail
}