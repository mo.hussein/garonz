const models = require('../database/models');


const answerQuestion = async (req, res) => {
    const {userId, questionId, text} = req.body;
    console.log(req.body);
    try {
        const answer = await models.Answer.create(req.body);
        return res.status(201).json({
            answer,
        });
    } catch (error) {
        return res.status(500).json({error: error.message})
    }
};
module.exports = {
 answerQuestion
}