const options = {
  swaggerDefinition: {
    info: {
      description: 'Garonz',
      title: 'Swagger',
      version: '1.0.0'
    },
    host: 'localhost:3000',
    basePath: '/api',
    produces: [
      'application/json',
      'application/xml'
    ],
    schemes: ['http']
  },
  basedir: __dirname, // app absolute path
  files: ['../routes/index.js'] // Path to the API handle folder
}

module.exports = options
