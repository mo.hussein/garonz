const { Router } = require('express');
const userController = require('../controllers/userController');
const router = Router();

router.get('/users/:userId', userController.getUserById);

module.exports = router;