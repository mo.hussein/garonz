const { Router } = require('express');
const userController = require('../controllers/userController');
const questionController = require('../controllers/questionController');
const router = Router();

router.get('/', (req, res) => res.send('Welcome'))
/**
 * @route GET /users
 * @group users - Get user details
 * @param {string} id.path.required - ID - eg: 1
 * @returns {object} 200 - user details
 * @returns {Error} 404  - Not a valid entry found for provided ID
 */

router.get('/users', userController.getUsers);
/**
 * @route GET /users/{id}
 * @group users - Get users details
 * @returns {object} 200 - eg {
 * "user": {
        "name": "Mohamed Hussein",
        "email": "M.Hussein@garonz.com",
        "id": 1,
        "answers": [
            {
                "text": "Answer second question...",
                "noOfQuestions": "1"
            }
        ]
    }
 * @returns {Error} 404  - User with the specified ID does not exists
 */
router.get('/users/:id', userController.getUserById);
router.post('/answers', questionController.answerQuestion);
module.exports = router;